#pragma once
#include <vector>

namespace P118
{
    class Solution
    {
    public:
        std::vector<std::vector<int>> generate(int numRows)
        {
            std::vector<std::vector<int>> matrix;

            if (numRows == 0) return matrix;

            matrix.push_back(std::vector<int>{1});

            for (auto i = 1; i < numRows; i++)
            {
                std::vector<int> row;
                row.reserve(i + 1);

                row.push_back(1);

                for (auto j = 0; j < i - 1; j++)
                {
                    row.push_back(matrix[i - 1][j] + matrix[i - 1][j + 1]);
                }

                row.push_back(1);

                matrix.push_back(row);
            }

            return matrix;
        }
    };
}
