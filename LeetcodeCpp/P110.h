#pragma once
#include "Tree.h"
#include <cmath>

namespace P110
{
    /**
    * Definition for a binary tree node.
    * struct TreeNode {
    *     int val;
    *     TreeNode *left;
    *     TreeNode *right;
    *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
    * };
    */
    class Solution
    {
    public:
        bool isBalanced(TreeNode* root)
        {
            return checkDepth(root) >= 0;
        }

    private:
        int checkDepth(const TreeNode *node)
        {
            if (!node) return 0;

            int leftDepth;
            int rightDepth;

            leftDepth = checkDepth(node->left);
            if (leftDepth == -1) return -1;

            rightDepth = checkDepth(node->right);
            if (rightDepth == -1) return -1;

            if (abs(leftDepth - rightDepth) <= 1)
            {
                return leftDepth > rightDepth ? leftDepth + 1 : rightDepth + 1;
            }
            else
            {
                return -1;
            }
        }
    };
}
