#pragma once
#include <vector>

namespace P119
{
    class Solution
    {
    public:
        std::vector<int> getRow(int rowIndex)
        {
            std::vector<int> row(rowIndex + 1);
            row[0] = 1;

            for (auto i = 1; i <= rowIndex; i++)
            {
                auto prev = 1;
                for (auto j = 1; j < i; j++)
                {
                    auto newPrev = row[j];
                    row[j] += prev;
                    prev = newPrev;
                }
                row[i] = 1;
            }

            return row;
        }
    };
}
