#pragma once
#include "Tree.h"
#include <queue>

namespace P111
{
    struct NodeDepth
    {
        TreeNode *node;
        int depth;
    };
    /**
    * Definition for a binary tree node.
    * struct TreeNode {
    *     int val;
    *     TreeNode *left;
    *     TreeNode *right;
    *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
    * };
    */
    class Solution
    {
    public:
        int minDepth(TreeNode* root)
        {
            if (!root) return 0;

            std::queue<NodeDepth> nodeQ;

            nodeQ.push(NodeDepth{ root, 1 });

            while (!nodeQ.empty())
            {
                auto nodeDepth = nodeQ.front();
                auto *node = nodeDepth.node;
                nodeQ.pop();

                if (!node->left && !node->right)
                {
                    return nodeDepth.depth;
                }

                if (node->left)
                {
                    nodeQ.push(NodeDepth{ node->left, nodeDepth.depth + 1 });
                }
                if (node->right)
                {
                    nodeQ.push(NodeDepth{ node->right, nodeDepth.depth + 1 });
                }
            }

            return -1;
        }
    };
}
