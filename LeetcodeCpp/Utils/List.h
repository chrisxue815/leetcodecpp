#pragma once

#include <initializer_list>

struct ListNode
{
    int val;
    ListNode *next;

    explicit ListNode(int x) : val(x), next(nullptr) {}

    static bool equals(const ListNode *head1, const ListNode *head2)
    {
        if (head1 == head2) return true;

        while (head1)
        {
            if (!head2 || head1->val != head2->val) return false;
            head1 = head1->next;
            head2 = head2->next;
        }

        return !head2;
    }
};

template<typename T>
class List
{
public:
    ListNode *head;

    List(std::initializer_list<T> list)
    {
        if (list.size() == 0)
        {
            head = nullptr;
            return;
        }

        auto iter = list.begin();
        head = new ListNode(*iter);
        ++iter;
        auto *cur = head;

        for (; iter != list.end(); ++iter)
        {
            cur->next = new ListNode(*iter);
            cur = cur->next;
        }
    }

    ~List()
    {
        while (head)
        {
            auto *toBeDeleted = head;
            head = head->next;
            delete(toBeDeleted);
        }
    }
};
