#pragma once
#include "Tree.h"

namespace P112
{
    /**
    * Definition for a binary tree node.
    * struct TreeNode {
    *     int val;
    *     TreeNode *left;
    *     TreeNode *right;
    *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
    * };
    */
    class Solution
    {
    public:
        bool hasPathSum(TreeNode* root, int sum)
        {
            if (!root) return false;

            return hasPathSum2(root, sum);
        }

        bool hasPathSum2(TreeNode* node, int sum)
        {
            if (!node->left && !node->right)
            {
                return sum == node->val;
            }

            if (sum <= 0) return false;

            return node->left && hasPathSum2(node->left, sum - node->val)
                || node->right && hasPathSum2(node->right, sum - node->val);
        }
    };
}
