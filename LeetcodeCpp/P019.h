#pragma once
#include "Utils/List.h"

namespace P019
{
    /**
    * Definition for singly-linked list.
    * struct ListNode {
    *     int val;
    *     ListNode *next;
    *     ListNode(int x) : val(x), next(NULL) {}
    * };
    */
    class Solution {
    public:
        ListNode* removeNthFromEnd(ListNode* head, int n)
        {
            auto *right = head;
            right = nth(right, n);

            if (!right)
            {
                auto newHead = head->next;
                delete(head);
                return newHead;
            }

            right = right->next;
            auto *left = head;

            while (right)
            {
                left = left->next;
                right = right->next;
            }

            auto *toBeRemoved = left->next;
            left->next = toBeRemoved->next;
            delete(toBeRemoved);
            return head;
        }

    private:
        static ListNode* nth(ListNode* node, int n)
        {
            for (auto i = 0; i < n; i++)
            {
                node = node->next;
            }
            return node;
        }
    };
}
