#include "stdafx.h"
#include "CppUnitTest.h"
#include <P019.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace LeetcodeCppTest
{
    TEST_CLASS(P019Test)
    {
    private:
        P019::Solution solution;

    public:
        TEST_METHOD(Test)
        {
            testResult({ 0, 1, 2, 3 }, 1, { 0, 1, 2 });
            testResult({ 0, 1, 2, 3 }, 2, { 0, 1, 3 });
            testResult({ 0, 1, 2, 3 }, 4, { 1, 2, 3 });
        }

    private:
        void testResult(std::initializer_list<int> input, int n, std::initializer_list<int> result)
        {
            List<int> inputList(input);
            List<int> resultList(result);

            inputList.head = solution.removeNthFromEnd(inputList.head, n);

            Assert::IsTrue(ListNode::equals(inputList.head, resultList.head));
        }
    };
}
